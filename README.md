# URL lookup service
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/jzafran/url-lookup/master)

Implementation of a URL lookup API.

## API
A single endpoint has been implemented to check if a specified URL (hostname/port and path) is known as malware.
See OpenAPI/Swagger UI (available at `public-url/swagger` - see email for URL) for details.

    GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}

## Deployment
Gitlab CI/CD automatically runs tests and then builds/deploys a docker container image to ECS upon merges to `master`. 
See [.gitlab-ci.yml](./.gitlab-ci.yml) and [Dockerfile](./Dockerfile).

## URL Blocklist
The URL blocklist is loaded into memory from `/app/blocklist.json` when the API server is initialized. By default,
this is a copy of [blocklist.json](./blocklist.json) (saved in the container image), but a different file can be used 
by setting the `URL_LOOKUP_BLOCKLIST` environment variable.

It's not feasible to re-build/deploy every time the list needs to be updated (especially for a large scale deployment),
so other another method would need to be implemented.

At the most basic level, a Docker volume/bind mount could be used to map `/app/blocklist.json` in the container 
to either a file on the Docker host's disk or a remote location, such as S3. Once the file has been updated, a
signal or API endpoint could notify the application to reload the blocklist file. Care would need to be taken to
ensure the API servers continue to service clients during the update.
  
Alternatively, an endpoint could be introduced for updating the blocklist. This would be pretty straightforward to
implement, but introduces complexity around scaling.

All of these implementations have another major weakness: the URL blocklist is limited by memory capacity of container host.
However, the blocklist source could be moved to an external database - Redis, DynamoDB, or really any
scalable key-value store.   

If support for wildcards is required (for the ability to block entire domains/hostnames) and external databases
introduce too much latency, there are many, well-defined 
[string pattern matching algorithms](https://www.cs.purdue.edu/homes/ayg/CS251/slides/chap11.pdf) that could possibly
be used as well.

Implementing any of these, would simply require one to implement the [`urllookup.Store` interface](./internal/urllookup/store.go).
