FROM golang:1.15-alpine3.12 AS builder
WORKDIR /go/src/app
COPY . .
RUN go get -d -v ./...
RUN go build .


FROM alpine:3.12
RUN addgroup -S app && \
    adduser -S app -G app
WORKDIR /app
COPY blocklist.json /app/blocklist.json
COPY --from=builder /go/src/app/url-lookup /app/url-lookup
ENV URL_LOOKUP_BLOCKLIST=/app/blocklist.json
ENV URL_LOOKUP_ADDRESS=0.0.0.0:8080
EXPOSE 8080
USER app
ENTRYPOINT ["/app/url-lookup"]
CMD []
