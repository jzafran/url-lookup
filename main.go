package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	"os"
	"time"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "addr",
				Usage:   "IP and port for server to listen on, in the form 'host:port'.",
				EnvVars: []string{"URL_LOOKUP_ADDRESS"},
				Value:   "127.0.0.1:8080",
			},
			&cli.PathFlag{
				Name:    "blocklist",
				Usage:   "Path to blocklist JSON file",
				EnvVars: []string{"URL_LOOKUP_BLOCKLIST"},
				Value:   "blocklist.json",
			},
			&cli.PathFlag{
				Name:      "tls-cert",
				Usage:     "Path to TLS certificate file",
				EnvVars:   []string{"URL_LOOKUP_TLS_CERTIFICATE"},
				TakesFile: true,
			},
			&cli.PathFlag{
				Name:      "tls-key",
				Usage:     "Path to TLS key file",
				EnvVars:   []string{"URL_LOOKUP_TLS_KEY"},
				TakesFile: true,
			},
		},
		Action: func(c *cli.Context) error {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
			// pretty-print logs to console when not running in ECS
			if os.Getenv("ECS_CONTAINER_METADATA_URI") == "" {
				log.Logger = zerolog.New(zerolog.ConsoleWriter{
					Out:        os.Stdout,
					TimeFormat: time.RFC3339,
				}).With().Timestamp().Logger()
			} else {
				zerolog.TimeFieldFormat = time.RFC3339
			}

			addr := c.String("addr")
			blocklistFile := c.Path("blocklist")
			tlsCert := c.Path("tls-cert")
			tlsKey := c.Path("tls-key")

			return RunServer(addr, blocklistFile, tlsCert, tlsKey)
		},
		Authors: []*cli.Author{
			{
				Name:  "Jeremy Zafran",
				Email: "jeremy@zafran.dev",
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
