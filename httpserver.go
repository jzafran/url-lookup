package main

import (
	"crypto/tls"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"github.com/unrolled/secure"
	"gitlab.com/jzafran/url-lookup/internal/api"
	"gitlab.com/jzafran/url-lookup/internal/urllookup"
	"gitlab.com/jzafran/url-lookup/internal/urllookup/memorystore"
	"net/http"
)

// @title urlinfo API
// @version 1.0
// @securityDefinitions.basic BasicAuth
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func RunServer(addr, blocklistFile, tlsCert, tlsKey string) error {
	malwareUrls, err := urllookup.ReadBlocklistFile(blocklistFile)
	if err != nil {
		return err
	}
	datastore := memorystore.New(malwareUrls...)
	lookupSvc := urllookup.New(datastore)

	r := gin.Default()
	r.Use(Secure())

	// fixme api.Use(cors.Default())
	api.SetupRoutes(r, lookupSvc)

	if tlsCert != "" && tlsKey != "" {
		server := &http.Server{
			Addr:    addr,
			Handler: r,
			TLSConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
			},
		}
		log.Info().Str("address", addr).Msg("listening and serving HTTPS")
		return server.ListenAndServeTLS(tlsCert, tlsKey)
	}

	return r.Run(addr)
}

// Secure is middleware for setting security-related HTTP headers.
func Secure() gin.HandlerFunc {
	middleware := secure.New(secure.Options{
		//AllowedHosts: []string{},
		//AllowedHostsAreRegex: false,
		FrameDeny:             true, // X-Frame-Options = DENY
		ContentTypeNosniff:    true, // X-Content-Type-Options = nosniff
		BrowserXssFilter:      true, // X-XSS-Protection = `1; mode=block`
		ContentSecurityPolicy: "default-src 'self'; img-src data: 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; font-src https://fonts.gstatic.com; object-src 'none'; frame-ancestors 'none'",
		//IsDevelopment: false,
	})

	return func(c *gin.Context) {
		if err := middleware.Process(c.Writer, c.Request); err != nil {
			c.Abort()
			return
		}
		// Avoid header rewrite if response is a redirection.
		if status := c.Writer.Status(); status > 300 && status < 399 {
			c.Abort()
		}
	}
}
