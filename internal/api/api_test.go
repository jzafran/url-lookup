package api

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
	"gitlab.com/jzafran/url-lookup/internal/urllookup"
	"gitlab.com/jzafran/url-lookup/internal/urllookup/memorystore"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

const testcasesFilePath = "../../testdata/blocklist.json"

func TestAPIUrlInfo(t *testing.T) {
	t.Parallel()

	// read test URLs from file and set up API server
	var testcases []struct {
		URL      string     `json:"url"`
		Host     string     `json:"host"`
		Path     string     `json:"path"`
		RawQuery string     `json:"raw_query"`
		Query    url.Values `json:"query"`
	}
	fc, err := ioutil.ReadFile(testcasesFilePath)
	require.NoError(t, err)
	require.NoError(t, json.Unmarshal(fc, &testcases))
	blocklist, err := urllookup.ReadBlocklistFile(testcasesFilePath)
	assert.NoError(t, err)
	assert.Len(t, blocklist, 18)
	datastore := memorystore.New(blocklist...)
	lookupSvc := urllookup.New(datastore)
	router := gin.New()
	SetupRoutes(router, lookupSvc)

	for _, tc := range testcases {
		t.Run(tc.URL, func(t *testing.T) {
			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/urlinfo/1/"+tc.URL, nil)
			router.ServeHTTP(w, req)
			assert.Equal(t, 200, w.Code)
			assert.JSONEq(t, `{"is_malicious":true}`, w.Body.String())
		})
	}
}
