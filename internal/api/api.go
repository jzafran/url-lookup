package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	_ "gitlab.com/jzafran/url-lookup/docs"
	"gitlab.com/jzafran/url-lookup/internal/urllookup"
	"net/http"
	"strings"
)

type URLInfoResponse struct {
	IsMalicious bool `json:"is_malicious"`
} //@name URLInfoResponse

type ErrorResponse struct {
	Code    int    `json:"code" example:"400"`
	Message string `json:"message" example:"status bad request"`
} //@name ErrorResponse

func NewErrorResponse(statusCode int) *ErrorResponse {
	return &ErrorResponse{
		Code:    statusCode,
		Message: http.StatusText(statusCode),
	}
}

// urlinfo godoc
// @Summary Check if URL is malicious
// @ID urlinfo
// @Produce  json
// @Param hostname_and_port path string true "Host and port"
// @Param path_and_query path string false "URL path and query" default()
// @Success 200 {object} URLInfoResponse
// @Failure 500 {object} ErrorResponse
// @Router /urlinfo/1/{hostname_and_port}/{path_and_query} [get]
func getURLInfo(lookupSvc urllookup.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		u := strings.TrimPrefix(c.Param("url"), "/")
		if u == "" {
			c.JSON(http.StatusNotFound, NewErrorResponse(http.StatusNotFound))
		} else if info, err := lookupSvc.GetURLInfo(u); err != nil {
			if errors.Is(err, urllookup.DoesNotExistError) {
				c.JSON(http.StatusOK, &URLInfoResponse{IsMalicious: false})
			} else {
				log.Error().Err(err).Str("url", u).Msg("error checking url")
				c.JSON(http.StatusInternalServerError, NewErrorResponse(http.StatusInternalServerError))
			}
		} else {
			c.JSON(http.StatusOK, &URLInfoResponse{IsMalicious: info.IsMalicious})
		}
	}
}

func SetupRoutes(r gin.IRouter, lookupSvc urllookup.Service) {
	r.GET("/urlinfo/1/*url", getURLInfo(lookupSvc))

	// use ginSwagger middleware to serve the API docs
	swaggerHandler := ginSwagger.WrapHandler(swaggerFiles.Handler, ginSwagger.URL("/swagger/doc.json"))
	r.GET("/swagger/*any", func(c *gin.Context) {
		if c.Request.URL.Path == "/swagger/" {
			c.Redirect(http.StatusTemporaryRedirect, "/swagger/index.html")
		} else {
			swaggerHandler(c)
		}
	})
}
