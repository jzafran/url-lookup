package memorystore

import (
	"gitlab.com/jzafran/url-lookup/internal/urllookup"
)

type Store struct {
	u map[urllookup.URL]urllookup.URLInfo
}

func New(maliciousUrls ...urllookup.URL) urllookup.Store {
	u := make(map[urllookup.URL]urllookup.URLInfo, 0)
	for _, url := range maliciousUrls {
		u[url] = urllookup.URLInfo{IsMalicious: true}
	}
	return &Store{u}
}

func (c *Store) GetURLInfo(u urllookup.URL) (*urllookup.URLInfo, error) {
	if info, exists := c.u[u]; exists {
		return &info, nil
	}
	return nil, urllookup.DoesNotExistError
}

var _ urllookup.Store = (*Store)(nil)
