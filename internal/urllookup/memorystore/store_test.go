package memorystore_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/jzafran/url-lookup/internal/urllookup"
	"gitlab.com/jzafran/url-lookup/internal/urllookup/memorystore"
	"testing"
)

func TestStore(t *testing.T) {
	t.Parallel()

	maliciousUrls := []urllookup.URL{
		{
			Host: "badwebsite.com",
			Path: "/take-bitcoin",
		},
		{
			Host: "www.otherbadsite.com",
			Path: "/foo/bar.s",
		},
		{
			Host: "www.abc1234.com:443",
			Path: "/foo/bar.s",
		},
		{
			Host: "ss.zzz.com:443",
			Path: "/foo/bar.s?e=2",
		},
		{
			Host: "ss.sdfh.com:443",
			Path: "",
		},
	}

	store := memorystore.New(maliciousUrls...)

	// verify that known-malicious URLs are returned as such
	for _, u := range maliciousUrls {
		t.Run(u.String(), func(t *testing.T) {
			info, err := store.GetURLInfo(u)
			assert.NoError(t, err)
			assert.Equal(t, &urllookup.URLInfo{IsMalicious: true}, info)
		})
	}

	// verify non-malicious URL
	info, err := store.GetURLInfo(urllookup.URL{Host: "goodsite.com", Path: "/login"})
	assert.EqualError(t, err, urllookup.DoesNotExistError.Error())
	assert.Nil(t, info)
}
