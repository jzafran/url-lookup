package urllookup

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseURL(t *testing.T) {
	t.Parallel()

	var testCases = []struct {
		raw      string
		expected URL
	}{
		{
			raw: "badwebsite.com/take-bitcoin",
			expected: URL{
				Host: "badwebsite.com",
				Path: "/take-bitcoin",
			},
		},
		{
			raw: "badwebsite.com",
			expected: URL{
				Host: "badwebsite.com",
				Path: "/",
			},
		},
		{
			raw: "www.abc1234.com:443/foo/bar.s",
			expected: URL{
				Host: "www.abc1234.com:443",
				Path: "/foo/bar.s",
			},
		},
		{
			raw: "ss.zzz.com:443/foo/bar.s?e=2",
			expected: URL{
				Host: "ss.zzz.com:443",
				Path: "/foo/bar.s?e=2",
			},
		},
		{
			raw: "ss.zzz.com:443/foo/bar.s?e=2",
			expected: URL{
				Host: "ss.zzz.com:443",
				Path: "/foo/bar.s?e=2",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.raw, func(t *testing.T) {
			u, err := ParseURL(tc.raw)
			assert.NoError(t, err)
			assert.Equal(t, tc.expected, *u)
		})
	}
}

func TestParseURL_invalid(t *testing.T) {
	t.Parallel()

	var testCases = []string{"", "/path"}

	for _, tc := range testCases {
		t.Run(tc, func(t *testing.T) {
			u, err := ParseURL(tc)
			assert.EqualError(t, err, InvalidURLError.Error())
			assert.Nil(t, u)
		})
	}
}
