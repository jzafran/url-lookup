package urllookup

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
)

// URL represents a URL (hostname, port, path, query) to check.
type URL struct {
	// hostname:port
	Host string `json:"host"`

	// path, with leading '/'
	Path string `json:"path"`
}

func (u *URL) String() string {
	return fmt.Sprintf("%s%s", u.Host, u.Path)
}

var InvalidURLError = errors.New("invalid URL")

// ParseURL parses a raw URL into a URL struct.
//
// The raw URL must be in the form `hostname[:port][/path]`. InvalidURLError
// will be returned when it cannot be parsed.
func ParseURL(u string) (*URL, error) {
	var url *URL
	s := strings.SplitN(u, "/", 2)
	if len(s) == 0 {
		return nil, InvalidURLError
	}
	if s[0] == "" {
		return nil, InvalidURLError
	}
	var path string
	if len(s) > 1 {
		path = s[1]
	}
	url = &URL{
		Host: normalizeHost(s[0]),
		Path: normalizePath(path),
	}
	return url, nil
}

func normalizeHost(host string) string {
	return strings.ToLower(host)
}

func normalizePath(path string) string {
	if !strings.HasPrefix(path, "/") {
		return "/" + path
	}
	return path
}

func ReadBlocklistFile(jsonPath string) ([]URL, error) {
	r, err := os.Open(jsonPath)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	malwareUrls := make([]URL, 0)
	if err := json.NewDecoder(r).Decode(&malwareUrls); err != nil {
		return nil, err
	}
	log.Info().Str("blocklistPath", jsonPath).Int("numberOfURLs", len(malwareUrls)).Msg("read blocklist file")
	return malwareUrls, nil
}
