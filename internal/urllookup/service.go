package urllookup

// URLInfo represents metadata for a specific URL (hostname, port, path, query).
type URLInfo struct {
	IsMalicious bool
}

type Service interface {
	GetURLInfo(url string) (*URLInfo, error)
}

type service struct {
	store Store
}

var _ Service = (*service)(nil)

func New(store Store) Service {
	return &service{
		store: store,
	}
}

// GetURLInfo
// InvalidURLError, DoesNotExistError
func (s *service) GetURLInfo(url string) (*URLInfo, error) {
	u, err := ParseURL(url)
	if err != nil {
		return nil, err
	}

	return s.store.GetURLInfo(*u)
}
