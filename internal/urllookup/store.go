package urllookup

import (
	"errors"
)

var DoesNotExistError = errors.New("does not exist")

type Store interface {
	// GetURLInfo
	// DoesNotExistError
	// The Check method is used to get metadata for a URL (hostname, port, path, query). It
	// must return nil as *Info if URL is not known.
	GetURLInfo(url URL) (*URLInfo, error)
}
